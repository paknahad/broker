package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

type Storage interface {
	Run(context.Context, chan string, chan string)
}

type FileStore struct {
	messagesFile *os.File
	readOnlyMessagesFile *os.File
	lastMessageFile *os.File
}

func NewStorage(messagesFile string, lastMessageFile string) *FileStore {
	lastLineFile, err := os.OpenFile(lastMessageFile, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	msgFile, err := os.OpenFile(messagesFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Panic(err)
	}
	rMsgFile, err := os.OpenFile(messagesFile, os.O_RDONLY, 0644)
	if err != nil {
		log.Panic(err)
	}

	return &FileStore{
		messagesFile: msgFile,
		readOnlyMessagesFile: rMsgFile,
		lastMessageFile: lastLineFile,
	}
}

func (s *FileStore) Run(ctx context.Context, msgChan chan string, sendChan chan string) {
	newMsg := make(chan bool)
	sendDone := make(chan bool)
	go s.Store(ctx, msgChan, newMsg, sendDone)
	go s.Load(ctx, newMsg, sendDone, sendChan)
	newMsg <- true // checking for unsent messages
}

func (s *FileStore) Store(ctx context.Context, msgChan chan string, newMsg chan bool, sendDone chan bool)  {
	defer s.messagesFile.Close()
	for {
		select {
		case <-ctx.Done():
			println("shutdown storage writer ...")
			return
		case msg := <-msgChan:
			s.messagesFile.WriteString(msg + "\n")
			select {
			case <- sendDone:
				newMsg <- true
			default:
			}
		}
	}
}

func (s *FileStore) Load(ctx context.Context, newMsg chan bool, sendDone chan bool, sendChan chan string) {
	defer s.lastMessageFile.Close()
	lastLine := loadLastLineHasBeenServed(s.lastMessageFile)
	lineNum := int64(0)
	for {
		select {
		case <-ctx.Done():
			println("shutdown storage loader ...")
			return
		case <-newMsg:
			sc := bufio.NewScanner(s.readOnlyMessagesFile)
			for sc.Scan() {
				lineNum++
				if lastLine >= lineNum {
					//println("----------break--------")
					continue
				}
				sendChan <- sc.Text()
				_, err := s.lastMessageFile.WriteAt([]byte(fmt.Sprintf("%d", lineNum)), 0) // Write at 0 beginning
				if err != nil {
					log.Fatalf("failed writing to file: %s", err)
				}
			}
			lastLine = lineNum
			sendDone <- true
			time.Sleep(200 * time.Nanosecond)
		}
	}
}

func loadLastLineHasBeenServed(file *os.File) int64 {
	buf := new(bytes.Buffer)
	buf.ReadFrom(file)
	lastLine, err := strconv.ParseInt(buf.String(), 10, 64)
	if err != nil {
		//log.Print("Error on loading last message number", err)
		lastLine = 0
	}
	if lastLine > 0 {
		println("start serving messages from :", lastLine)
	}

	return lastLine
}
