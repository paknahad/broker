package main

import (
	"context"
	zmq "github.com/pebbe/zmq4"
	"log"
)

type Broker interface {
	Connect() error
	Close() error
	Start()
}

type ZMQBroker struct {
	receiver *zmq.Socket
	sender   *zmq.Socket
}

func New() *ZMQBroker {
	receiver, _ := zmq.NewSocket(zmq.PULL)
	sender, _ := zmq.NewSocket(zmq.PUSH)
	return &ZMQBroker {
		receiver: receiver,
		sender: sender,
	}
}

func (z *ZMQBroker) Close() error {
	err1 := z.receiver.Close()
	if err1 != nil {
		log.Println(err1)
	}
	err2 := z.sender.Close()
	if err2 != nil {
		log.Println(err2)
		return err2
	}

	return err1
}

func (z *ZMQBroker) Connect(listenAddress string, sendAddress string) error {
	err := z.receiver.Connect(listenAddress)
	if err != nil {
		return err
	}

	return z.sender.Bind(sendAddress)
}

func (z *ZMQBroker) Start(ctx context.Context, storage Storage) {
	msgChan := make(chan string)
	sendChan := make(chan string)
	storage.Run(ctx, msgChan, sendChan)
	go z.send(ctx, sendChan)
	for {
		select {
		case <-ctx.Done():
			println("shutdown broker ...")
			return
		default:
			task, err := z.receiver.Recv(zmq.DONTWAIT)
			if err != nil {
				break
			}
			msgChan <- task
		}
	}
}

func (z *ZMQBroker) send(ctx context.Context, sendChan chan string)  {
	for {
		select {
		case msg := <-sendChan:
			_, err := z.sender.Send(msg, 0)
			if err != nil {
				log.Println(err)
			}
		case <-ctx.Done():
			println("shutdown sender ...")
			return
		}
	}
}
