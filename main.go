package main

import (
	"context"
	"log"
	"os"
	"os/signal"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	go func() {
		oscall := <-sigChan
		log.Printf("system call:%+v", oscall)
		cancel()
	}()

	b := New()
	err := b.Connect("tcp://localhost:5557", "tcp://*:5558")
	if err != nil {
		log.Panicln(err)
		return
	}
	defer b.Close()

	storage := NewStorage("/tmp/messages", "/tmp/last_message")

	b.Start(ctx, storage)
}
